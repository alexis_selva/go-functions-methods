These are the programming assignments relative to Go - Functions, Methods, and Interfaces

- Assignment 1: create a routine that sorts a series of ten integers using the bubble sort algorithm
- Assignment 2: create a routine that solves a linear kinematics problem
- Assignment 3: programmatically get information about a set of predefined objects
- Assignment 4: write a GoLang routine that allows users to create a set of animals and then get information about those animals

For more information, I invite you to have a look at https://www.coursera.org/learn/golang-functions-methods
