package main

import ( 
	"fmt"
	"strings"
)

type Animal struct {
	food string
	locomotion string
	noise string
}

func (a *Animal) InitAnimal(name string) int {
	switch {
		case strings.Compare(name, "cow") == 0:
			a.food = "grass"
			a.locomotion = "walk"
			a.noise = "moo"
		case strings.Compare(name, "bird") == 0:
			a.food = "worms"
			a.locomotion = "fly"
			a.noise = "peep"
		case strings.Compare(name, "snake") == 0:
			a.food = "mice"
			a.locomotion = "slithler"
			a.noise = "hss"
		default:
			return -1
	}

	return 0	
}

func (a *Animal) Eat() {
	fmt.Println(a.food)
}

func (a *Animal) Move() {
	fmt.Println(a.locomotion)
}

func (a *Animal) Speak() {
	fmt.Println(a.noise)
}

func (a *Animal) DoAction(action string) int {
	switch {
		case strings.Compare(action, "eat") == 0:
			a.Eat()
		case strings.Compare(action, "move") == 0:
			a.Move()
		case strings.Compare(action, "speak") == 0:
			a.Speak()
		default:
			return -1
	}
	
	return 0
}

func main() {
	var name string
	var action string
	var animal Animal	

	// For loop
	for {

		// Read the animal
		fmt.Print("> ")
		_, err := fmt.Scanf("%s %s", &name, &action)
		if err != nil {
			fmt.Println(err)
			continue
		}

		// Initialize the animal
		ret := animal.InitAnimal(name)
		if ret != 0 {
			fmt.Println("Unknown animal: " + name)
			continue
		}
	
		ret = animal.DoAction(action)
		if ret != 0 {
			fmt.Println("Invalid action: " + action)
			continue
		}
	}
}
