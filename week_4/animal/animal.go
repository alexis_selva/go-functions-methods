package main

import ( 
	"fmt"
	"strings"
	"errors"
)

type Animal interface {
	Eat()
	Move()
	Speak()
}

type AnimalStruct struct {
	name string
}

type Cow AnimalStruct

func (t Cow) Eat() {
	fmt.Println("grass")
}

func (t Cow) Move() {
	fmt.Println("walk")
}

func (t Cow) Speak() {
	fmt.Println("moo")
}

type Bird AnimalStruct

func (t Bird) Eat() {
	fmt.Println("worms")
}

func (t Bird) Move() {
	fmt.Println("fly")
}

func (t Bird) Speak() {
	fmt.Println("peep")
}

type Snake AnimalStruct

func (t Snake) Eat() {
	fmt.Println("mice")
}

func (t Snake) Move() {
	fmt.Println("slithler")
}

func (t Snake) Speak() {
	fmt.Println("hss")
}

func NewAnimal(name, animal string) (Animal, error) {
	switch {
		case strings.Compare(animal, "cow") == 0:
			return Cow{name}, nil
		case strings.Compare(animal, "bird") == 0:
			return Bird{name}, nil
		case strings.Compare(animal, "snake") == 0:
			return Snake{name}, nil
	}

	return nil, errors.New("Unknown animal type !")
}

func QueryAnimal(a Animal, action string) error {
	switch {
		case strings.Compare(action, "eat") == 0:
			a.Eat()
		case strings.Compare(action, "move") == 0:
			a.Move()
		case strings.Compare(action, "speak") == 0:
			a.Speak()
		default:
			return errors.New("Unknown action !")
		
	}
	
	return nil
}

func main() {
	var command string
	var name string
	var action string
	var animals map[string] Animal
	var animal Animal

	// Init the map
	animals = make(map[string] Animal)	

	// For loop
	for {

		// Read the animal
		fmt.Print("> ")
		_, err := fmt.Scanf("%s %s %s", &command, &name, &action)
		if err != nil {
			fmt.Println(err)
			continue
		}

		switch {
			case strings.Compare(command, "newanimal") == 0:

				// Create a new animal
				animal, err := NewAnimal(name, action)
				if err != nil {
					fmt.Println(err)
					continue
				}
				animals[name] = animal
				fmt.Print("Created it!\n")

			case strings.Compare(command, "query") == 0:

				// Get the created animal	
				animal = animals[name]
				if animal == nil {
					fmt.Printf("Unknown animal name !\n")
					continue
				}

				// Query the info of the created animal
				err := QueryAnimal(animal, action)
				if err != nil {
					fmt.Println(err)
					continue
				}
			default:
				fmt.Printf("Unknown command: %s\n", command)
		}
	}
}
