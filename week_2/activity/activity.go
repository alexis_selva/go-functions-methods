package main

import ( 
	"fmt"
)

// Compute displacement as a function of time
func GenDisplayFn(a, v0, s0 float64) func (float64) float64 {
	fn := func (t float64) float64 {
		return 0.5 * a * t * t + v0 * t + s0	
	}
	return fn
}

func main() {
	var a float64
	var v0 float64
	var s0 float64
	var t float64

	// Read acceleration	
	fmt.Print("Enter the acceleration: ")
	_, err := fmt.Scanf("%f", &a)
	if err != nil {
		fmt.Println(err)
		return
	} 
	
	// Read initial velocity
	fmt.Print("Enter the initial velocity: ")
	_, err = fmt.Scanf("%f", &v0)
	if err != nil {
		fmt.Println(err)
		return
	} 
	
	// Read initial displacement
	fmt.Print("Enter the initial displacement: ")
	_, err = fmt.Scanf("%f", &s0)
	if err != nil {
		fmt.Println(err)
		return
	} 
	
	// Read time
	fmt.Print("Enter the time: ")
	_, err = fmt.Scanf("%f", &t)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Implement displacement function
	fn := GenDisplayFn(a, v0, s0)

	// Print displacement	
	fmt.Println(fn(t))
}
