package main

import ( 
	"fmt"
)

// XOR swap
func swap(sli []int, i int) {  
    sli[i] = sli[i] ^ sli[i + 1]  
    sli[i + 1] = sli[i + 1] ^ sli[i]  
    sli[i] = sli[i] ^ sli[i + 1]  
}  
  
// Bubble sort algorithm
func bubbleSort(sli []int) {
	for i:= 0; i < len(sli) - 1; i++ {
		for j:= 0; j < len(sli) - i - 1 ; j++ {
			if (sli[j] > sli[j+1]) { 
				swap(sli, j);  
			}  
		}
	}
}

func main() {
	var value int

	// Create an empty slice of size 10
	slice := make([]int, 10)

	// Slice the slice
	slice = slice[:0]

	// For loop
	for i := 1; i <= 10; i++ {

		// Read an integer	
		fmt.Print("Enter an integer: ")
		_, err := fmt.Scanf("%d", &value)
		if err != nil {
			fmt.Println(err)
			continue
		}

		// Add the integer into the slice
		slice = append(slice, value)
	}

	// Sort the slice
	bubbleSort(slice)

	// Print the content of the slice in sorted order
	fmt.Printf("[%d]int{", len(slice))
	fmt.Print(slice)
	fmt.Println("}")
}
